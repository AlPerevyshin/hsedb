# Parallel buffer pool manager

Как вы, вероятно, заметили в предыдущей задаче, один экземпляр диспетчера пула буферов должен использовать `mutex`, чтобы быть потокобезопасным. Это может вызвать `contention`, поскольку каждый поток борется за один `mutex` при взаимодействии с пулом буферов. Одним из возможных решений является наличие в вашей системе нескольких пулов буферов.

`ParallelBufferPoolManager` — это класс, который содержит несколько экземпляров `BufferPoolManagerInstances`. Для каждой операции `ParallelBufferPoolManager` выбирает один экземпляр `BufferPoolManagerInstance` и делегирует его этому экземпляру.

Мы можем использовать идентификатор страницы, чтобы определить, какой конкретный `BufferPoolManagerInstance` использовать. Если у нас есть `num_instances` `BufferPoolManagerInstances`, то нам нужен какой-то способ сопоставить данный идентификатор страницы с числом в диапазоне [0, num_instances). Для этого проекта мы будем использовать оператор по модулю, `page_id` mod `num_instances` сопоставит данный `page_id` с правильным диапазоном.

При первом создании экземпляра `ParallelBufferPoolManager` его начальный индекс должен быть равен 0. Каждый раз, когда вы создаете новую страницу, вы будете пытаться создать страницу используя следующий `BufferPoolManagerInstance`, начиная с начального индекса, пока один из них не будет успешным.

Убедитесь, что при создании отдельных экземпляров `BufferPoolManagerInstances` вы используете конструктор, который принимает uint32_t `num_instances` и uint32_t `instance_index`, чтобы идентификаторы страниц создавались правильно.

Вам потребуется реализовать следующие функции, определенные в заголовочном файле (`src/buffer/parallel_buffer_pool_manager.h`) в исходном файле (`src/buffer/parallel_buffer_pool_manager.cpp`):

- ParallelBufferPoolManager(num_instances, pool_size, disk_manager, log_manager)
- ~ParallelBufferPoolManager()
- GetPoolSize()
- GetBufferPoolManager(page_id)
- FetchPgImp(page_id)
- UnpinPgImp(page_id, is_dirty)
- FlushPgImp(page_id)
- NewPgImp(page_id)
- DeletePgImp(page_id)
- FlushAllPagesImpl()

Ссылка на оригинальное описание задачи https://15445.courses.cs.cmu.edu/fall2021/project1/.
