# Database Systems Course

---

## Ссылки

- [Репозиторий с лекциями](https://github.com/kitaisreal/db-course-hse)
- [Репозиторий с лабораторными по SQL](https://github.com/kitaisreal/DBLabs)
- [Страница курса на wiki ФКН](http://wiki.cs.hse.ru/Database_Systems_2022)

## Инструкции

1) [Настройка окружения](docs/setup.md)
2) [Как сдавать задачи](docs/ci.md)

## Навигация

- [Manytask](https://hsedb.manytask.org)
- [Результаты](https://docs.google.com/spreadsheets/d/1tJ0FdaukKi25aaLPdNRM9KTECYX8Gx6exxYi1kTLjUY/)
