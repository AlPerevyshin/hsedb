//===----------------------------------------------------------------------===//
//
//                         BusTub
//
// clock_replacer_test.cpp
//
// Identification: test/buffer/clock_replacer_test.cpp
//
// Copyright (c) 2015-2019, Carnegie Mellon University Database Group
//
//===----------------------------------------------------------------------===//

#include <cstdio>
#include <thread>  // NOLINT
#include <vector>

#include "buffer/clock_replacer.h"
#include "gtest/gtest.h"

namespace bustub {

// NOLINTNEXTLINE
TEST(ClockReplacerTest, SampleTest) {
  ClockReplacer clock_replacer(7);

  // Scenario: unpin six elements, i.e. add them to the replacer.
  clock_replacer.Unpin(1);
  clock_replacer.Unpin(2);
  clock_replacer.Unpin(3);
  clock_replacer.Unpin(4);
  clock_replacer.Unpin(5);
  clock_replacer.Unpin(6);
  clock_replacer.Unpin(1);
  EXPECT_EQ(6, clock_replacer.Size());

  // Scenario: get three victims from the clock.
  int value;
  EXPECT_TRUE(clock_replacer.Victim(&value));
  EXPECT_EQ(1, value);
  EXPECT_TRUE(clock_replacer.Victim(&value));
  EXPECT_EQ(2, value);
  EXPECT_TRUE(clock_replacer.Victim(&value));
  EXPECT_EQ(3, value);

  // Scenario: pin elements in the replacer.
  // Note that 3 has already been victimized, so pinning 3 should have no effect.
  clock_replacer.Pin(3);
  clock_replacer.Pin(4);
  EXPECT_EQ(2, clock_replacer.Size());

  // Scenario: unpin 4. We expect that the reference bit of 4 will be set to 1.
  clock_replacer.Unpin(4);

  // Scenario: continue looking for victims. We expect these victims.
  EXPECT_TRUE(clock_replacer.Victim(&value));
  EXPECT_EQ(5, value);
  EXPECT_TRUE(clock_replacer.Victim(&value));
  EXPECT_EQ(6, value);
  EXPECT_TRUE(clock_replacer.Victim(&value));
  EXPECT_EQ(4, value);
  EXPECT_FALSE(clock_replacer.Victim(&value));
}


// NOLINTNEXTLINE
TEST(ClockReplacerTest, SizeTest) {
  ClockReplacer replacer(5);

  for (frame_id_t i = 0; i < 10; ++i) {
    replacer.Unpin(i);
  }

  EXPECT_EQ(5, replacer.Size());
}


// NOLINTNEXTLINE
TEST(ClockReplacerTest, Stress) {
  enum class What {
    Victim = 0,
    Pin,
    Unpin
  };

  const int replacer_size = 1'000'000;

  ClockReplacer replacer(replacer_size);

  for (size_t attempt = 0; attempt < 10'000'000; ++attempt) {
    // NOLINTNEXTLINE (rand)
    auto what = static_cast<What>(rand() % 3);
    // NOLINTNEXTLINE (rand)
    frame_id_t frame_id = rand() % replacer_size;

    switch (what) {
      case What::Victim: {
        replacer.Victim(&frame_id);
        ASSERT_TRUE(frame_id >= 0 && frame_id < replacer_size);
        break;
      }
      case What::Unpin: {
        replacer.Unpin(frame_id);
        break;
      }
      case What::Pin: {
        replacer.Pin(frame_id);
        break;
      }
    }
  }

}

}  // namespace bustub
