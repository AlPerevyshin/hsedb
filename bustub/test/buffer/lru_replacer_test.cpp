//===----------------------------------------------------------------------===//
//
//                         BusTub
//
// lru_replacer_test.cpp
//
// Identification: test/buffer/lru_replacer_test.cpp
//
// Copyright (c) 2015-2019, Carnegie Mellon University Database Group
//
//===----------------------------------------------------------------------===//

#include <cstdio>
#include <thread>  // NOLINT
#include <vector>

#include "buffer/lru_replacer.h"
#include "gtest/gtest.h"

namespace bustub {

// NOLINTNEXTLINE
TEST(LRUReplacerTest, SampleTest) {
  LRUReplacer lru_replacer(7);

  // Scenario: unpin six elements, i.e. add them to the replacer.
  lru_replacer.Unpin(1);
  lru_replacer.Unpin(2);
  lru_replacer.Unpin(3);
  lru_replacer.Unpin(4);
  lru_replacer.Unpin(5);
  lru_replacer.Unpin(6);
  EXPECT_EQ(6, lru_replacer.Size());

  // Scenario: get three victims from the lru.
  int value;
  EXPECT_TRUE(lru_replacer.Victim(&value));
  EXPECT_EQ(1, value);
  EXPECT_TRUE(lru_replacer.Victim(&value));
  EXPECT_EQ(2, value);
  EXPECT_TRUE(lru_replacer.Victim(&value));
  EXPECT_EQ(3, value);

  // Scenario: pin elements in the replacer.
  // Note that 3 has already been victimized, so pinning 3 should have no effect.
  lru_replacer.Pin(3);
  lru_replacer.Pin(4);
  EXPECT_EQ(2, lru_replacer.Size());

  // Scenario: unpin 4. We expect that 4 will be the most recently used frame.
  lru_replacer.Unpin(4);

  // Scenario: continue looking for victims. We expect these victims.
  EXPECT_TRUE(lru_replacer.Victim(&value));
  EXPECT_EQ(5, value);
  EXPECT_TRUE(lru_replacer.Victim(&value));
  EXPECT_EQ(6, value);
  EXPECT_TRUE(lru_replacer.Victim(&value));
  EXPECT_EQ(4, value);
  EXPECT_FALSE(clock_replacer.Victim(&value));
}

// NOLINTNEXTLINE
TEST(LRUReplacerTest, SizeTest) {
  LRUReplacer replacer(5);

  for (frame_id_t i = 0; i < 10; ++i) {
    replacer.Unpin(i);
  }

  EXPECT_EQ(5, replacer.Size());
}


// NOLINTNEXTLINE
TEST(LRUReplacerTest, Stress) {
  enum class What {
    Victim = 0,
    Pin,
    Unpin
  };

  const int replacer_size = 1'000'000;

  LRUReplacer lru_replacer(replacer_size);

  for (size_t attempt = 0; attempt < 10'000'000; ++attempt) {
    // NOLINTNEXTLINE (rand)
    auto what = static_cast<What>(rand() % 3);
    // NOLINTNEXTLINE (rand)
    frame_id_t frame_id = rand() % replacer_size;

    switch (what) {
      case What::Victim: {
        lru_replacer.Victim(&frame_id);
        ASSERT_TRUE(frame_id >= 0 && frame_id < replacer_size);
        break;
      }
      case What::Unpin: {
        lru_replacer.Unpin(frame_id);
        break;
      }
      case What::Pin: {
        lru_replacer.Pin(frame_id);
        break;
      }
    }
  }

}

}  // namespace bustub
